<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\DetailView
?>
<h1>Products details</h1>

<?=
DetailView::widget([
    'model' => $productModel,
    'attributes' => [
        'name',
        'description:html',
    ],
]);
?>
<hr>



<?=
GridView::widget([
    'dataProvider' => $addresses,
    'columns' => [
        'shopName',
        'regexp',
        'url',
        ['label' => 'Current price',
            'value' => function($data) {
                return $data->getProductPriceChanges()->orderBy(['date' => SORT_DESC] )->one()->value;
            }
        ],
        [
            'class' => yii\grid\ActionColumn::className(),
            'controller' => 'address',
        ],
    ]
]);
?>
<hr/>

<?= Html::a('Add Shop', ['/address/add'], ['class'=>'btn btn-primary']) ?>

erterter

