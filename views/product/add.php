<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Pjax::begin([]);
$form = ActiveForm::begin([
    'id' => 'add-product',
    'options' => ['class' => 'form-horizontal', 'data' => ['pjax' => true]]
]) ?>

<?= $form->field($productModel, 'name'); ?>
<?= $form->field($productModel, 'description'); ?>

<?= Html::submitButton('Save product', ['class' => 'btn btn-primary']) ?>


<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>