<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;
?>
<h1>Products list</h1>

<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        'description',
        [
            'class' => 'yii\grid\ActionColumn',
            
        ]
        ]
    ]); ?>

