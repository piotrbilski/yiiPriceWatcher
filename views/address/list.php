<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;
?>
<h1>Products list</h1>

<?php echo GridView::widget([
    'dataProvider' => $prices,
    'columns' => [
        'date',
        'value',
        [
            'class' => 'yii\grid\ActionColumn',
            
        ]
        ]
    ]); ?>

