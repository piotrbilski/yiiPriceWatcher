<?php

/*
 */

namespace app\models;

/**
 * Description of ProductAddressSearch
 *
 * @author a542959
 */
class ProductAddressSearch extends ProductAddress{
    //put your code here
    public function rules(){
        return [
            [['shopName', 'url','regexp'], 'safe'],
            
        ];
    }
    
    public function search($params){
        $query = ProductAddress::find();
        
        $dataProvider = new ActiveDataProvider(['query' => $query]);
        
        $this->load($params);
        
        if(!$this->validate()){
            return $dataProvider;
        }
        $query->andFilterWhere(['url' => $this->url]);
        $query->andFilterWhere(['like', 'regexp', $this->regexp])->
                andFilterWhere(['like', 'shopName', $this->shopName]);
                
        
        return $dataProvider;
    }
    
}
