<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productAddress".
 *
 * @property integer $idProductAddress
 * @property string $url
 * @property string $regexp
 * @property string $shopName
 * @property integer $product_idProduct
 *
 * @property Product $productIdProduct
 * @property ProductPriceChanges[] $productPriceChanges
 */
class ProductAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productAddress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shopName', 'product_idProduct'], 'required'],
            [['product_idProduct'], 'integer'],
            [['url', 'regexp', 'shopName'], 'string', 'max' => 45],
            [['product_idProduct'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_idProduct' => 'idProduct']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProductAddress' => 'Id Product Address',
            'url' => 'Url',
            'regexp' => 'Regexp',
            'shopName' => 'Shop Name',
            'product_idProduct' => 'Product Id Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIdProduct()
    {
        return $this->hasOne(Product::className(), ['idProduct' => 'product_idProduct']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPriceChanges()
    {
        return $this->hasMany(ProductPriceChanges::className(), ['productAddress_idProductAddress' => 'idProductAddress']);
    }
}
