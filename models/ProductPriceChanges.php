<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productPriceChanges".
 *
 * @property integer $idProductPrices
 * @property string $date
 * @property double $value
 * @property integer $productAddress_idProductAddress
 *
 * @property ProductAddress $productAddressIdProductAddress
 */
class ProductPriceChanges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productPriceChanges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProductPrices', 'value', 'productAddress_idProductAddress'], 'required'],
            [['idProductPrices', 'productAddress_idProductAddress'], 'integer'],
            [['value'], 'number'],
            [['date'], 'string', 'max' => 45],
            [['productAddress_idProductAddress'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAddress::className(), 'targetAttribute' => ['productAddress_idProductAddress' => 'idProductAddress']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProductPrices' => 'Id Product Prices',
            'date' => 'Date',
            'value' => 'Value',
            'productAddress_idProductAddress' => 'Product Address Id Product Address',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAddressIdProductAddress()
    {
        return $this->hasOne(ProductAddress::className(), ['idProductAddress' => 'productAddress_idProductAddress']);
    }
}
