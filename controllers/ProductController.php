<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Product;
use yii\data\ActiveDataProvider;

class ProductController extends Controller{
    
    public function actionList($message = 'test'){
        
        $query = Product::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        
        
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            
                ]);
    }
    
    public function actionAdd(){
        
        $product = new Product();
        
        if($product->load(Yii::$app->request->post()) && $product->validate()){
            $product->save();
        }
        
        return $this->render('add', ['productModel' => $product]);
    }
    
    public function actionView($id=null){
        
        if(!$id){
            return $this->redirect(['product/list']);
        }
        
        $product = Product::findOne($id);
        $addresses = new ActiveDataProvider(['query' => $product->getProductAddresses()]);
        $addressesSearch = new \app\models\ProductAddressSearch();
        return $this->render('view', ['productModel' => $product, 'addresses' => $addresses, 'addressesSearch' => $addressesSearch]);
    }
    
    public function actionUpdate($id){
        
        $product = Product::findOne($id);
        
        if($product->load(Yii::$app->request->post()) && $product->validate()){
            $product->save();
            return $this->redirect(['product/list']);
            
        }
        
        if($product){
            return $this->render('add', ['productModel' => $product]);
        }
        
        
    }
    
    public function actionDelete($id){
       
            $productToDelete = Product::findOne($id);
            
            if($productToDelete){
                
            $productToDelete->delete();
        }           
        
        
        return $this->redirect(['product/list']);
    }
    
}