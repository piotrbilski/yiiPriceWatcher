<?php

/*
 */

namespace app\controllers;
use Yii;
use yii\web\Controller;
use app\models\ProductAddress;
use yii\data\ActiveDataProvider;

class AddressController extends Controller{
    
    public function actionList($id = null){
        
        if(!$id){
            $this->redirect(['product/list']);
        }
        
        $address = ProductAddress::findOne($id);
        $prices = null;
        if(!$address){
            $this->redirect(['product/list']);
        }
        $prices = $address->getProductPriceChanges();
        $dataProvider = new ActiveDataProvider([
            'query' => $prices
        ]);
        
        
        return $this->render('list', ['prices' => $dataProvider]);
        
    }
    
    public function actionAdd(){
        
    }
    
    public function actionView($id){
        return $this->actionList($id);
    }
}